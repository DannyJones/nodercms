var _ = require('lodash');
var async = require('async');
var orderModel = require('../models/order.model');
var logger = require('../../lib/logger.lib');

exports.one = function (options, callback) {
    if (!options.user._id) {
        var err = {
            type: 'system',
            error: '没有 _id 传入'
        };
        return callback(err);
    }
    var _id = options.user._id;
    orderModel.find()
        .exec(function (err, order) {
            if (err) {
                err.type = 'database'
                return callback(err)
            }
            callback(null, order)
        })
}

exports.all = function (callback) {
    orderModel.find({})
        .lean()
        .exec(function (err, order) {
            if (err) {
                err.type = 'database';
                return callback(err);
            }
            callback(null, order);
        });
};

exports.save = function (options, callback) {
    if (!options.data) {
        var err = {
            type: 'system',
            error: '没有传入 data'
        };
        return callback(err);
    }

    var data = options.data;
    new orderModel(data).save(function (err, user) {
        if (err) {
            err.type = 'database';
            return callback(err);
        }
        callback(null, user);
    });
};
