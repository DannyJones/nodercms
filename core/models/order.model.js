var mongoose = require('mongoose');

/**
 * 订单模型
 */
var ordersSchema = new mongoose.Schema({
  userid: String,
  HouseType: String,
  AddressDetail:String,
  HouseArea: String,
  FitClassify: String,
  FitStyle: String,
  Budget: String,
  IsModeling: String,
  IsNeedWorkingPlan: String,
  IsNeedMeasure: String,
  HouseImagePath: String,
  authorities: [Number]
}, {
    collection: 'order',
    id: false
  });

/**
 * 发布为模型
 */
module.exports = mongoose.model('order', ordersSchema);