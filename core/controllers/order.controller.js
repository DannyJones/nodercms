var _ = require('lodash');
var async = require('async');
var logger = require('../../lib/logger.lib');
var orderService = require('../services/order.service');
var pagesService = require('../services/pages.service');//查询单页信息渲染
var siteInfoService = require('../services/site-info.service');
var categoriesService = require('../services/categories.service');


var moment = require('moment')
var fs = require('fs')
var multer = require('multer')
var upload = multer({ dest: "../../public" }).any()


module.exports = {
    //查询所有..未判断权限
    all: function (req, res) {
        if (req.session.user == '') {

        }
        orderService.all(function (err, order) {
            if (err) {
                logger[err.type]().error(err);
                return res.status(500).end();
            }
            res.status(200).json(order);
        });
    },
    //查询自己的订单,跳转个人信息页面
    list: function (req, res) {
        var _id = req.session.user;

        orderService.one({ _id: _id }, function (err, order) {
            if (err) {
                logger[err.type]().error(err);
                return res.status(500).end();
            }
            return res.status(200).json(order);
        })
        //res.status(200).json('test');
    },
    //创建订单
    create: function (req, res) {
        logger.system().info('提交订单');
        var user = req.session.user;
        logger.system().info(user);

        logger.system().info('检查图片')


        var targetfile = ''
        upload(req, res, function (err) {
            if (err) {
                return res.status(400).json({ err: err })
            }
            req.file = req.files[0]
            var tmp_path = req.file.path
            var targetpath = 'public/media/houseimages/'
            var now = moment().format('YYYYMMDDHHmmss')
            targetfile = targetpath + now + '_' + req.file.originalname

            var src = fs.createReadStream(tmp_path)
            console.log('src ' + tmp_path)
            var dest = fs.createWriteStream(targetfile)
            console.log('target ' + targetfile)
            src.pipe(dest)
            src.on('end', function () {
                var data = {
                    userid: user,
                    HouseType: req.body.HouseType,
                    HouseArea: req.body.HouseArea,
                    FitClassify: req.body.FitClassify,
                    FitStyle: req.body.FitStyle,
                    Budget: req.body.Budget,
                    IsModeling: req.body.IsModeling,
                    IsNeedWorkingPlan: req.body.IsNeedWorkingPlan,
                    IsNeedMeasure: req.body.IsNeedMeasure,
                    HouseImagePath: targetfile,
                    AddressDetail: req.body.AddressDetail
                }
                orderService.save({ data: data }, function (err, order) {
                    if (err) {
                        logger[err.type]().error(__filename, err);
                        return res.status(500).end();
                    }
                    logger.system().info('存储' + order);
                    logger.system().info('准备渲染页面');


                    //res.location('/hyfw/hyxx').end()
                    res.redirect('/hyfw/hyxx')

                    //res.status(200).json('test');er
                    // pagesService.one({
                    //     path: '/hyfw/hyxx'
                    // }, function (err, page) {
                    //     if (err) return res.status(500).end();
                    //     if (!page) return next();
                    //     // 读取单页所需数据
                    //     async.parallel({
                    //         siteInfo: siteInfoService.get,
                    //         navigation: function (callback) {
                    //             categoriesService.navigation({ current: page.path }, callback);
                    //         }
                    //     }, function (err, results) {
                    //         res.render(page.views.page, {
                    //             layout: page.views.layout,
                    //             siteInfo: results.siteInfo,
                    //             navigation: results.navigation,
                    //             category: page,
                    //             content: page.mixed.pageContent,
                    //             media: page.mixed.pageMedia,
                    //         });
                    //     });
                    // });
                })
            })
            src.on('error', function (err) {
                res.end()
                logger.system().error(err)
            })
        })

        //return res.status(200).json({ msg: 'test' })

    }
};

